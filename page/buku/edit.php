<?php 
	$id = $_GET["id"];
	$edit = $mysqli->query("SELECT * FROM tb_buku WHERE id =$id");
	$e = mysqli_fetch_array($edit);
 ?>

 <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Data obat
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">	
								 <form name="form_buku" action="?page=buku&aksi=update" method="post" enctype="multipart/form-data">
								 	<div class="form-group">
										<label for="judul">Nama Obat</label>
										<input type="hidden" name="id" value="<?= $e["id"]; ?>">
								 		<input type="text" class="form-control" id="judul" name="judul" value="<?= $e["judul"]; ?>" required>
								 	</div>

								 	<div class="form-group">
								 		<label for="pengarang">Diagnosa Penyakit</label>
								 		<input type="text" class="form-control" id="pengarang" name="pengarang" value="<?= $e["pengarang"]; ?>" required>
								 	</div>

								 	<div class="form-group">
								 		<label for="penerbit">Jenis Obat</label>
								 		<select name="penerbit" class="form-control" id="penerbit" name="penerbit" required>
										 <option value="<?= $e["penerbit"]; ?>"><?= $e["penerbit"]; ?></option>
										 <option value="pil">pil</option>
													<option value="sirup">sirup</option>
													<option value="tablate">tablate</option>
													<option value="salap">salap</option>
													<option value="minyak urut">minyak urut</option>
										</select>
								 	</div>
		                         
								 	<div class="form-group">
												<label for="tahun_terbit">Expired</label>
												<input type="date" class="form-control" id="tahun_terbit"  name="tahun_terbit" required>
												
											</div>
									</div>

									<div class="form-group">
										<label for="jumlah_buku">Jumlah Obat</label>
										<input type="number" class="form-control" id="jumlah_buku" name="jumlah_buku" value="<?= $e["jumlah_buku"]; ?>" required>
									</div>

								 	<div class="form-group">
								 		<label for="gambar">Gambar</label>
								 		<input type="file" class="form-control" id="gambar" name="gambar">
								 		<span><?= $e["gambar"]; ?></span>
								 	</div>

								 	<div class="form-group">
								 		<button type="reset" class="btn btn-danger">Reset</button>
										<button type="submit" class="btn btn-primary">Perbarui</button>
								 	</div>

								 </form>