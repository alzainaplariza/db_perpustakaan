<div class="panel panel-default">
                        <div class="panel-heading">
                            Tambah Data Obat
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    	<form name="form_buku" action="?page=buku&aksi=create" method="POST" enctype="multipart/form-data">
											<div class="form-group">
												<label for="judul">Nama Obat</label>
												<input type="text" class="form-control" id="judul" placeholder="Masukan Nama Obat" name="judul" required>
											</div>

											<div class="form-group">
												<label for="pengarang">Diagnosa Penyakit</label>
												<input type="text" class="form-control" id="pengarang" placeholder="Masukan Diagnosa penyakit" name="pengarang" required>
											</div>

											<div class="form-group">
												<label for="penerbit">Jenis Obat</label>
												<select class="form-control" id="penerbit" placeholder="Masukan Dosis" name="penerbit" required>
													<option value="">none</option>
													<option value="pil">pil</option>
													<option value="sirup">sirup</option>
													<option value="tablate">tablate</option>
													<option value="salap">salap</option>
													<option value="minyak urut">minyak urut</option>
												</select>
											

											<div class="form-group">
												<label for="tahun_terbit">Expired</label>
												<input type="date" class="form-control"id="tahun_terbit"  name="tahun_terbit" required>
												
											</div>

											<div class="form-group">
												<label for="jumlah_buku">Jumlah Obat</label>
												<input type="number" class="form-control" id="jumlah_buku" placeholder="Masukan Jumlah Obat" name="jumlah_buku" required>
											</div>


											<div class="form-group">
												<label for="Gambar">Gambar</label>
												<input type="file" class="form-control" id="gambar" name="gambar" required>
											</div>

											<div class="form-group">
												<button type="reset" class="btn btn-danger">Reset</button>
												<button type="submit" class="btn btn-primary">Simpan</button>
											</div>

										</form>
									</div>
								</div>
							</div>
						</div>